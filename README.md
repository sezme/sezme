Copyright 2017 Karsten Wade <quaid@iquaid.org>

This file is part of sezme.

sezme is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

sezme is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with sezme.  If not, see <http://www.gnu.org/licenses/>.

# Purpose of this program

This program maintains a password file encrypted with your GPG key.

Put all of your passwords in this file, and `sezme` handles the task of encrypting that file, then making searching for passwords and managing the file easier than doing it all manually.

Program functions include:

* `sezstart` - creates initial files and directories
* `sezvi` - open the password file for editing, and saves to an unencrypted temporary file
* `sezpush` - updates one or more remote copies of the encrypted password file
* `sezgrep` - uses `grep -i` to search the password file for a keyword
* `sezgrep-context` - uses `grep -A -B` to search the password file for a keyword; you set the value of the number of lines to display -B(efore) and -A(fter) the keyword.
* `sezless` - opens the password file in the `less` shell program
* `sezmake` - encrypts the temporary file to replace the existing (old) password file, then runs `shred -zu` on the temporary file
* `sezopen` - opens the password file to standard output (STDOUT)

# Requirements

Any computer system that has a bash-like shell with `gpg` and `vim` installed. For example, any Linux-based system (desktop, laptop, server, phone (Android), etc.), any OSX system, any Windows system with a bash emulator such as cygwin or PuTTy.

You must have `gpg` installed and configured for your personal key.

# How to install

* Copy src files to a location in your path such as `$HOME/bin` where you keep your personal (i.e., non-system) executable files.

# How to use

1. Run `sezstart` to create the initial files and directories.
1. Use `sezvi` to open your initial sezme.gpg file.
1. Populate file with URLs, usernames, and passwords.
1. Save this as a temporary file with an obvious name e.g. `sezme-unencrypted`
1. Use `sezmake` to convert `sezme-unencrypted` to the `sezme.gpg` file, then shreds the temporary file before exit.

```
   sezmake 
   Name of file to encrypt to sezme.gpg? sezme-unencrypted
   Backing up sezme.gpg file ...
   Encrypting sezme-unencrypted ...
   File `/home/kwade/.sezme/sezme.gpg' exists. Overwrite? (y/N) y
   Shredding sezme-unencrypted ...
   Shredded, dude!
   Done.
```

# Roadmap

For now, the roadmap for sezme is simple:

* Get it released;
* Tell other people;
* Get advice on improving the tool;
* Keep it a shell script for small footprint.

# Contact info

Karsten 'quaid' Wade <quaid@iquaid.org>
http://sezme.org
