#!/bin/bash
#
# Copyright 2011 - 2015 Karsten Wade <quaid@iquaid.org>
#
#    This file is part of sezme.
#
#    sezme is free software: you can redistribute it and/or modify it
#    under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    sezme is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with sezme.  If not, see <http://www.gnu.org/licenses/>.
#
# The 'sezgrep' script takes in a search string, searches for the
# string using 'grep', ignoring case, then dumps the results to the
# console plus the ten lines following the search result

# Fixed variables
VI="/usr/bin/vim"
GPG="/usr/bin/gpg"
GPG_OPTS="--decrypt"
GREP="/bin/grep"
GREP_OPTS="-i"
GREP_OPTSA="-A"
GREP_OPTSB="-B"
ECHO="/bin/echo"
ECHO_OPTS="-en"
SEZME_FILE="$HOME/.sezme/sezme.gpg"

# Get the search string from the user:
$ECHO $ECHO_OPTS "String? "
read STRING
$ECHO $ECHO_OPTS "How many lines (B)efore to display? "
read BLINES
$ECHO $ECHO_OPTS "How many lines (A)fter to display? "
read ALINES
# Search for the string and number of lines before and after, then send to standard output (STDOUT)
$GPG $GPG_OPTS $SEZME_FILE | $GREP $GREP_OPTS $GREP_OPTSB $BLINES $GREP_OPTSA $ALINES $STRING
# Make it clear we are done:
$ECHO " "
$ECHO "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
$ECHO "Done!"
$ECHO " "
$ECHO "................................................................."
